#! bash -uvx
cwd=`pwd`
project=`basename $cwd`
rm -rf build/libs/*.jar
gradle shadowjar
exewrap-$MSYSTEM_CARCH -e "SINGLE;NOLOG" -o ./${project}-all.exe build/libs/${project}-all.jar
./${project}-all.exe

